<?php

/**
 * @file
 * Entity destination handler for Better Migrate.
 *
 * Depends on the migrate_extras module!
 *
 * @todo Trigger errors where necessary.
 */

/**
 * Entity destination handler for Better Migate.
 *
 * Requires two properties or arguments:
 * 1. Entity Type
 *  The entity type of the destination entity.
 *  - Property: $entityType
 *  - Argument: entity_type
 * 2. Entity Bundle
 *  The bundle of the destination entity.
 *  - Property: $entityBundle
 *  - Arugment: entity_bundle
 */
trait BetterMigrationDestinationEntity {
  protected function initDestination() {
    return new MigrateDestinationEntityAPI(
      $this->getEntityType(),
      $this->getEntityBundle(),
      $this->getDestinationOptions()
    );
  }

  protected function getEntityType() {
    if (!empty($this->arguments['destination_options']['entity_type'])) {
      return $this->arguments['destination_options']['entity_type'];
    }
    if (!empty($this->entityType)) {
      return $this->entityType;
    }
    // trigger error
  }

  protected function getEntityBundle() {
    if (!empty($this->arguments['destination_options']['entity_bundle'])) {
      return $this->arguments['destination_options']['entity_bundle'];
    }
    if (!empty($this->entityBundle)) {
      return $this->entityBundle;
    }
    // trigger error
  }

  protected function getDestinationKeySchema() {
    return MigrateDestinationEntityAPI::getKeySchema(
      $this->getEntityType(),
      $this->getEntityBundle()
    );
  }
}
