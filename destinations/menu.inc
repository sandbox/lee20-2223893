<?php

/**
 * @file
 * Migrate trais for Drupal menus and menu links.
 */

/**
 * Destination trait for menus.
 */
trait BetterMigrateDestinationMenu {
  protected function initDestination() {
    return new MigrateDestinationMenu();
  }
  protected function getDestinationKeySchema() {
    return MigrateDestinationMenu::getKeySchema();
  }
}

/**
 * Destination trait for menu links.
 */
trait BetterMigrateDestinationMenuLinks {
  protected function initDestination() {
    return new MigrateDestinationMenuLinks();
  }
  protected function getDestinationKeySchema() {
    return MigrateDestinationMenuLinks::getKeySchema();
  }
}

/**
 * When migrating menu links from one Drupal site to another, where you
 * already have the relevant menu data stubbing for the menu_link plid and
 * level fields (p1-p9) causes erratic behavior.
 *
 * In these cases it is ideal to do a simple Table migration. This class
 * and the corresponding trait are provided to help highlight this issue and
 * save you from beating your head on the wall.
 *
 * This class might be improved upon to support more cases where stubbing
 * is involved but perhaps the data isn't identical.
 *
 * The stubbing issue can be countered with MigrateDestinationMenuLinks by not
 * stubbing running the migration multiple times.
 *
 * @see BettermigateDestinationMenuLinksTable
 * @see menu_link_save()
 */
class MigrateDestinationMenuLinksTable extends MigrateDestinationTable {

  /**
   * Replicate the fields from MigrateDesintationMenuLinks.
   */
  public function fields($migration = NULL) {
    $x = new MigrateDestinationMenuLinks();
    return $x->fields($migration);
  }

  public function prepare($menu_link, stdClass $row) {
    parent::prepare($menu_link, $row);
    // Serialize the link options as late as possible
    $menu_link->options = serialize($menu_link->options);
  }
}

trait BetterMigrateDestinationMenuLinksTable {
  protected function initDestination() {
    return new MigrateDestinationMenuLinksTable('menu_links');
  }
  protected function getDestinationKeySchema() {
    return MigrateDestinationMenuLinks::getKeySchema();
  }
  protected function createStub(Migration $migration, array $source_ids) {
    $stub_id = db_insert('menu_links')->fields(array(
      'link_title' => t('Stub for @source_id', array('@source_id' => $source_ids[0])),
    ))->execute();
    return $stub_id ? array($stub_id) : FALSE;
  }
}
