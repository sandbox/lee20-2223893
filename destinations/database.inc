<?php

/**
 * @file
 */

trait BetterMigrateDestinationDatabase {
  protected function getSourceIDField() {
    if (!isset($this->sourceIDField)) {
      if (isset($this->arguments['source_id_field'])) {
        $this->sourceIDField = $this->arguments['source_id_field'];
      }
      else {
        throw new MigrateException(t('No source id field received @migration', array(
          '@migration' => $this->machineName,
        )));
      }
    }
    return $this->sourceIDField;
  }
}
