<?php

/**
 * @file
 * SQL mapping for Better Migrate.
 *
 * @todo Need documentation and usage examples.
 */

trait BetterMigrateMapSQL {
  use BetterMigrateDestinationDatabase;

  abstract protected function getMappingFields();
  abstract protected function getDestinationKeySchema();

  protected function initMap() {
    return new MigrateSQLMap(
      $this->machineName,
      $this->getMappingFields(),
      $this->getDestinationKeySchema()
    );
  }
}

trait BetterMigrateMapSQLSimpleInteger {
  use BetterMigrateMapSQL;
  protected function getMappingFields() {
    return array(
      $this->getSourceIDField()=> array(
        'type' => 'int',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ),
    );
  }
}

trait BetterMigrateMapSQLSimpleString {
  use BetterMigrateMapSQL;
  protected function getMappingFields() {
    return array(
      $this->getSourceIDField() => array(
        'type' => 'varchar',
        'length' => $this->getMappingIDLength(),
        'not null' => TRUE,
      ),
    );
  }

  protected function getMappingIDLength() {
    if (!empty($this->arguments['map_id_length'])) {
      return $this->arguments['map_id_length'];
    }
    elseif (empty($this->mapIDLength)) {
      $this->mapIDLength = 255;
    }
    return $this->mapIDLength;
  }
}
