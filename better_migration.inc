<?php
/**
 * @file
 */

/**
 * BetterMigration base class.
 *
 * When only using the Migration class provided by the migrate module a lot
 * happens in the extending class constructors including:
 *  - Source initialization
 *  - Destination initialization
 *  - Map initialization
 *  - Field mappings
 *
 * With better migration, these tasks are abtracted into methods which
 * extending classes (your migrations) can implement explicitly or by using
 * traits.
 *
 * This allows your migrations to be smaller, cleaner, and more extensible.
 */
abstract class BetterMigration extends Migration {
  protected function getDataDirectory() {
    if (!empty($this->arguments['data_directory'])) {
      return $this->arguments['data_directory'];
    }
    if (empty($this->dataDirectory)) {
      $obj = new ReflectionClass($this);
      $this->dataDirectory = dirname($obj->getFileName());
    }
    return $this->dataDirectory;
  }

  protected function getDependency($key) {
    if (isset($this->dependencies[$key])) {
      return $this->dependencies[$key];
    }
  }

  /**
   * Abstract methods that should be implemented by extending classes.
   */
  abstract protected function initMap();
  abstract protected function initDestination();
  abstract protected function initSource();
  abstract protected function mapFields();

  /**
   * The intent of this class structure is so that you don't have to override
   * the constructor in your base classes. If you do run into a situation where
   * you need to override this, it is suggested that you continue to use the
   * initialization methods.
   */
  public function __construct($arguments = array()) {
    // Call the parent constructor to get initialized
    parent::__construct($arguments);

    // Translate our description if not empty, this allows us to define
    // our $description attribute as a class attribute instead of in the
    // constructor
    if (!empty($this->description)) {
      $this->description = t($this->description);
    }

    if (!empty($arguments['dependencies'])) {
      $this->dependencies = $arguments['dependencies'];
    }

    // Initialize our source, destination, and map using methods so that
    // the relevant logic can be easily overridden without having to override
    // the entire constructor
    $this->destination = $this->initDestination();
    $this->source      = $this->initSource();
    $this->map         = $this->initMap();

    // Invoke our field mapping method
    $this->mapFields();
  }

  protected function getSourceOptions() {
    if (!empty($this->arguments['source_options'])) {
      $this->sourceOptions = $this->arguments['source_options'];
    }
    if (empty($this->sourceOptions)) {
      $this->sourceOptions = array();
    }
    return $this->sourceOptions;
  }

  protected function getDestinationOptions() {
    if (!empty($this->arguments['destination_options'])) {
      $this->destinationOptions = $this->arguments['destination_options'];
    }
    if (empty($this->destinationOptions)) {
      $this->destinationOptions = array();
    }
    return $this->destinationOptions;
  }
}
