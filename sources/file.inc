<?php

/**
 * @file
 */

/**
 * Abstract trait (doesn't implement initSource())
 */
trait BetterMigrateSourceFile {
  protected function getSourceDirectory() {
    if (!isset($this->sourceDirectory)) {
      if (isset($this->arguments['source_directory'])) {
        $this->sourceDirectory = $this->arguments['source_directory'];
      }
      else {
        $obj = new ReflectionClass($this);
        $this->sourceDirectory = dirname($obj->getFileName());
      }
    }
    return $this->sourceDirectory;
  }

  protected function getSourceFilename() {
    if (!isset($this->sourceFilename)) {
      if (isset($this->arguments['source_filename'])) {
        $this->sourceFilename = $this->arguments['source_filename'];
      }
    }
    return $this->sourceFilename;
  }

  protected function getSourceFilepath() {
    return $this->getSourceDirectory() . '/' . $this->getSourceFilename();
  }
}
