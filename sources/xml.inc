<?php

/**
 * @file
 * XML Source handler for use with BetterMigration.
 */

/**
 * Source a migration with XML.
 *
 * Requires 3 properties or arguments:
 *
 * 1. XML Filename
 *  - Property: $xmlFilename
 *  - Argument: xml_filename
 *  Eg. my-source-file.xml
 * 2. Item Xpath
 *  The xpath to each element
 *  - Property: $itemXpath
 *  - Argument: item_xpath
 *  Eg. /items/item
 * 3. Item Xpath ID
 *  The name of the tag containing the each item's id
 *  - Property: $itemXpathID
 *  - Argument: item_xpath_id
 *  Eg. id
 */
trait BetterMigrationSourceXML {
  use BetterMigrateSourceFile;

  /**
   * Get the item xpath from the extending class' $xmlFilename attribute or
   * from an argument.
   */
  protected function getItemXpath() {
    if (!empty($this->arguments['item_xpath'])) {
      $this->itemXpath = $this->arguments['item_xpath'];
    }
    if (!empty($this->itemXpath)) {
      return $this->itemXpath;
    }
    // trigger error
  }

  protected function getItemXpathID() {
    if (!empty($this->arguments['item_xpath_id'])) {
      $this->itemXpathID = $this->arguments['item_xpath_id'];
    }
    if (!empty($this->itemXpathID)) {
      return $this->itemXpathID;
    }
    // trigger error
  }

  /**
   * Abstract method, classes using this trait must define the XML fields using
   * this method.
   *
   * @return Array
   *   An array describing the XML fields
   */
  abstract protected function sourceFields();

  protected function getFields() {
    // Allow fields to be overridden with arguments
    if (!empty($this->arguments['source_fields'])) {
      return $this->arguments['source_fields'];
    }
    return $this->sourceFields();
  }

  protected function initSource() {
    return new MigrateSourceXML(
      $this->getSourceFilepath(),
      $this->getItemXpath(),
      $this->getItemXpathID(),
      $this->getFields(),
      $this->getSourceOptions()
    );
  }

  /**
   * Mappings carry over from MigrateSourceXML exept you don't have to do
   * any additional xpath mapping if your xpaths line up with the soureFields().
   *
   * @param string|null $destination_field
   *   machine-name of destination field
   * @param string|null $source_field
   *   name of source field
   * @param bool $warn_on_override
   *   Set to FALSE to prevent warnings when there's an existing mapping
   *   for this destination field.
   *
   * @return MigrateXMLFieldMapping
   *   MigrateXMLFieldMapping
   */
  public function addFieldMapping($destination_field, $source_field = NULL,
                                  $warn_on_override = TRUE) {
    // Warn of duplicate mappings.
    if ($warn_on_override && !is_null($destination_field) && isset($this->codedFieldMappings[$destination_field])) {
      self::displayMessage(
        t('!name addFieldMapping: !dest was previously mapped, overridden',
          array('!name' => $this->machineName, '!dest' => $destination_field)),
        'warning');
    }
    $mapping = new MigrateXMLFieldMapping($destination_field, $source_field);
    if (is_null($destination_field)) {
      $this->codedFieldMappings[] = $mapping;
    }
    else {
      $this->codedFieldMappings[$destination_field] = $mapping;
    }
    $mapping->xpath($source_field);
    return $mapping;
  }

  protected function applyMappings() {
    // We only know what data to pull from the xpaths in the mappings.
    foreach ($this->getFieldMappings() as $mapping) {
      $source = $mapping->getSourceField();
      if ($source && !isset($this->sourceValues->{$source})) {
        if (!$xpath = $mapping->getXpath()) {
          $xpath = $source;
        }
        if ($xpath) {
          // Derived class may override applyXpath().
          $source_value = $this->applyXpath($this->sourceValues, $xpath);
          if (!is_null($source_value)) {
            $this->sourceValues->$source = $source_value;
          }
        }
      }
    }
    parent::applyMappings();
  }

  /**
   * Need description here.
   *
   * @param stdClass $data_row
   *   row containing items.
   * @param string $xpath
   *   xpath used to find the item
   *
   * @return SimpleXMLElement
   *   found element
   */
  public function applyXpath($data_row, $xpath) {
    $result = $data_row->xml->xpath($xpath);
    if ($result) {
      if (count($result) > 1) {
        $return = array();
        foreach ($result as $record) {
          $return[] = (string) $record;
        }
        return $return;
      }
      else {
        return (string) $result[0];
      }
    }
    else {
      return NULL;
    }
  }
}
