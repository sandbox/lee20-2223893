<?php

trait BetterMigrateSourceJSON {
  use BetterMigrateSourceFile;

  protected function initSourceURLs() {
    switch (true) {
      case isset($this->sourceUrls):
        return $this->sourceUrls;
      case isset($this->arguments['source_urls']):
        return $this->arguments['source_urls'];
      default:
        return $this->getSourceDirectory() . '/' . $this->getSourceFilename();
    }
  }

  abstract function getSourceIDField();

  protected function initSource() {
    return new MigrateSourceJSON(
      $this->initSourceURLs(),
      $this->getSourceIDField(),
      $this->sourceFields(),
      $this->getSourceOptions()
    );
  }
}
